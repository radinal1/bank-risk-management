export const state = () => ({
  DETAIL_RISK_ALL: {},
  DETAIL_RISK_TITLE: "",
  DETAIL_RISK_CHOOSEN: "",
});

export const getters = {
  GET_DETAIL_RISK_TITLE: (state) => state.DETAIL_RISK_TITLE,
  GET_DETAIL_RISK_ALL: (state) => state.DETAIL_RISK_ALL,
  GET_DETAIL_RISK_CHOOSEN: (state) => state.DETAIL_RISK_CHOOSEN,
};

export const mutations = {
  ADD_DETAIL_RISK(state, payload) {
    state.DETAIL_RISK_TITLE = payload.title;

    let keyFound = [];
    Object.keys(payload).forEach((key) => {
      if (key === "title") return;
      if (key === "ejdesc") return;
      keyFound.push({
        key: key.split("_").join(" "),
        payload: payload[key],
      });
    });

    state.DETAIL_RISK_ALL = keyFound;
  },

  ADD_DETAIL_RISK_CHOOSEN(state, payload) {
    state.DETAIL_RISK_CHOOSEN = payload;
  },
};

export const actions = {
  ADD_DETAIL_RISK({ commit }, payload) {
    commit("ADD_DETAIL_RISK", payload);
  },
  ADD_DETAIL_RISK_CHOOSEN({ commit }, payload) {
    commit("ADD_DETAIL_RISK_CHOOSEN", payload);
  },
};
