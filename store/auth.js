export const state = () => ({
  USER: {},
});

export const getters = {
  GET_USER: (state) => state.USER,
};

export const mutations = {
  SET_AUTH(state, payload) {
    state.USER = payload;
  },
};

export const actions = {
  SET_AUTH({ commit }, payload) {
    commit("SET_AUTH", payload);
  },
};
