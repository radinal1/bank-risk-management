import axios from "axios";

const instance = axios.create({
  baseURL: process.env.NUXT_BASE_APP_URL,
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
  },
});
export default instance;
