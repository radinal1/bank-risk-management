import api from "./apiToken";

const INDIKATOR = "/dashboard/indikator";
const RISK_STATUS = "/dashboard/riskstatus";
const RISK_GRAPH = "/dashboard/risk-graph";

class DashboardServices {
  getIndikator() {
    return api.get(INDIKATOR);
  }
  getRiskStatus() {
    return api.get(RISK_STATUS);
  }
  getRiskGraph() {
    return api.get(RISK_GRAPH);
  }
}

export default new DashboardServices();
