import axios from "axios";

const fetchClient = () => {
  const instance = axios.create({
    baseURL: process.env.NUXT_BASE_APP_URL,
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  });

  instance.interceptors.request.use((conf) => {
    let token = window.$nuxt.$store.getters["auth/GET_USER"].token;
    conf.headers.Authorization = `Bearer ${token}`;
    return conf;
  });

  return instance;
};

export default fetchClient();
