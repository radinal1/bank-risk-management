import api from "./api";

const LOGIN = "/login";

class AuthServices {
  login(data) {
    return api.post(LOGIN, data);
  }
}

export default new AuthServices();
