import api from "./apiToken";

const RISK_LIST = "/dashboard/branch-risk";
const RISK_DETAIL = "/risks/branch-risk";
const RISK_INDICATOR = "/risks/indikator";

const RISK_ITEM_ID_9 = "/risks/activity_item_id2";
const RISK_ITEM_ID_10 = "/risks/activity_item_id";

const POST_RISK_CONDITION = "/risks/kondisi_singkat";
const RISK_ACTION_STEP = "/verification/action";
const POST_RISK_EVIDENCE = "/risks/upload-evidence";
const POST_HANDLING_RISK = "/risks/handling";

class DetailRiskServices {
  getRiskList() {
    return api.get(RISK_LIST);
  }
  getRiskDetail(params) {
    return api.get(RISK_DETAIL, { params });
  }
  getRiskIndikator(params) {
    return api.get(RISK_INDICATOR, { params });
  }

  getRiskItemID_9(params) {
    return api.get(RISK_ITEM_ID_9, { params });
  }
  getRiskItemID_10(params) {
    return api.get(RISK_ITEM_ID_10, { params });
  }

  postRiskCondition(data, params) {
    return api.post(POST_RISK_CONDITION, data, { params });
  }
  getActionStep(params) {
    return api.get(RISK_ACTION_STEP, { params });
  }
  postRiskEvidence(data) {
    return api.post(POST_RISK_EVIDENCE, data);
  }
  postHandlingRisk(data) {
    return api.post(POST_HANDLING_RISK, data);
  }
}

export default new DetailRiskServices();
