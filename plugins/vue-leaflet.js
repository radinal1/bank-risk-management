import Vue from "vue";
import { LMap, LTileLayer, LMarker } from "vue2-leaflet";
import LHeatmap from "@/components/LHeatMap.vue";

Vue.component("l-map", LMap);
Vue.component("l-tile-layer", LTileLayer);
Vue.component("l-marker", LMarker);
Vue.component("LHeatmap", LHeatmap);
