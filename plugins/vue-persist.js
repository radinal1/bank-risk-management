import VuexPersistence from "vuex-persist";

export default ({ store }) => {
  new VuexPersistence({
    /* your options */
    key: "bri-store",
    storage: window.localStorage,
    reducer: (state) => ({ auth: state.auth, risk: state.risk }),
  }).plugin(store);
};
