# Dockerfile
FROM node:16 as dependencies
WORKDIR /bricommando
COPY package.json yarn.lock ./
RUN yarn install

FROM node:16-alpine as builder
WORKDIR /bricommando
COPY . .
ADD .env.example .env
COPY --from=dependencies /bricommando/node_modules ./node_modules
RUN yarn build
RUN yarn generate

EXPOSE 8101

ENV NUXT_HOST=0.0.0.0
ENV NUXT_PORT=8101
CMD ["yarn", "start"]
