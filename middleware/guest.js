export default function ({ redirect, store }) {
  const isAuthenticated = store.state.auth.USER.token ? true : false;
  if (isAuthenticated) {
    redirect({ name: "admin" });
  }
}
